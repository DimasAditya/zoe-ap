package command;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        commands.stream().forEach(Command::execute);
    }

    @Override
    public void undo() {
        for (int commandIndex = commands.size() - 1; commandIndex >= 0; commandIndex--) {
            commands.get(commandIndex).undo();
        }
    }
}
