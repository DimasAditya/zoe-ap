package exercise1;

import command.CeilingFanCommand;
import command.CeilingFanHighCommand;
import command.CeilingFanMediumCommand;
import command.CeilingFanOffCommand;
import command.LightOffCommand;
import command.LightOnCommand;
import invoker.RemoteControlWithUndo;
import receiver.CeilingFan;
import receiver.Light;

public class RemoteLoader {

    public static void main(String[] args) {
        RemoteControlWithUndo remoteControl = new RemoteControlWithUndo();

        CeilingFan ceilingFan = new CeilingFan("Living Room");
        Light light = new Light("Kitchen");

        CeilingFanCommand ceilingFanMedium =
                new CeilingFanMediumCommand(ceilingFan);
        CeilingFanCommand ceilingFanHigh =
                new CeilingFanHighCommand(ceilingFan);
        CeilingFanCommand ceilingFanOff =
                new CeilingFanOffCommand(ceilingFan);

        LightOnCommand lightOn = new LightOnCommand(light);
        LightOffCommand lightOff = new LightOffCommand(light);

        remoteControl.setCommand(0, ceilingFanMedium, ceilingFanOff);
        remoteControl.setCommand(1, ceilingFanHigh, ceilingFanOff);
        remoteControl.setCommand(2, lightOn, lightOff);

        remoteControl.onButtonWasPushed(0);
        remoteControl.offButtonWasPushed(0);
        System.out.println(remoteControl);
        remoteControl.undoButtonWasPushed();

        remoteControl.onButtonWasPushed(1);
        System.out.println(remoteControl);
        remoteControl.undoButtonWasPushed();

        remoteControl.onButtonWasPushed(2);
        System.out.println(remoteControl);
        remoteControl.undoButtonWasPushed();
    }
}
